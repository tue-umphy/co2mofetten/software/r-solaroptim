#' @title  Declination
#' @description  The declination is the angular difference between the earth's eqator and the zenith of the sun (due to the ecliptic of the earth's axis)
#' It is used as a sub function for the cos_incidence function
#'
#' @reference lecture about the basics of regenerative energy from Jens Bange.
#'
#' @param time time interval of measurement [asPOSIXct]
#' @return Declination [Rad]
#'
#' @examples
#' #calculation of declination during a measurment period
#' time = mydata$time
#' declination(time)
#' plot(time,declination)
#' @export
#'
declination <- function(time,debug=FALSE){
  new_year <- as.POSIXct(strftime(min(time),format = "%Y-01-01 00:00:00 +0000"),format="%F %T %z")
  if(debug) cat("new year of ",strftime(min(time), format="%F %T %Z")," is ",strftime(new_year, format="%F %T %Z"),"\n")

  sec_since_new_year <- as.numeric(time)-as.numeric(new_year)
  if(debug) cat("sekunden seit new_year: ",sec_since_new_year[0], "\n")

  day_of_year <- sec_since_new_year/(24*3600)
  if(debug) cat("tag des Jahres ",day_of_year[0],"\n")

  declination <- (-23.44*pi/180) * cos(2*pi*(day_of_year+10)/365)

  return(declination)
}

#' @title Angle per second
#'
#' @description Calculation of the angle the earth rotates every second.
#' At local (depending on the longitude) solar time 12:00 pm the angle is defined as 0 degrees (Bange)
#' It is used as a sub function for the cos_incidence function
#'
#' @param time time interval of measurement [asPOSIXct]
#' @param lon longitude [Rad]
#' @return angle of the earth's rotation at a specific time [Rad]
#'
#' @examples
#' time <- as.POSIXct(mydata$time,format="%Y-%m-%d %H:%M:%S", tz="UTC")
#' angle_per_second(time,lon=8.8*pi/180)
#' plot(angle_per_second)
#'
#' @export

angle_per_second <- function(time,lon,debug=FALSE){
  seconds_off_12<- as.numeric(time) -as.numeric(as.POSIXct(strftime(time,format="%F 12:00:00 +0000"),format="%F %T %z"))
  if(debug) cat("offset since 12 o'clock (in sec)",seconds_off_12[0],"\n")

  angluar_speed<-2*pi/(24*3600)
  if(debug) cat("angular speed per second ",angluar_speed,"\n")

  angle_change_per_sec <- seconds_off_12 * angluar_speed + lon
  if(debug) cat("rotation of the earth since 12 o'clock [Rad] ",angle_change_per_sec[0],"\n")

  return(angle_change_per_sec)
}


#' @title Fast angle of incidence
#' @description A formula to make the fitting faster
#' The fast_cos_incidence is a formula to calculate the cosinus of the angle of incidence from direct sun radiation to a arbitrarily aligned surface.
#' It is used as a sub function for the cos_incidence function
#' Also useable on its own if declination and angle_per_second is calculated before
#'
#' @usage fast_cos_incidence(lat,lon,tilt,rot,declination,angle_per_second,debug=FALSE)
#'
#' @reference lecture about the basics of regenerative energy from Jens Bange.
#'
#' @param lat Latitude [Rad]
#' @param lon longitude [Rad]
#' @param tilt angle between solar panel and ground [Rad]
#' @param rot rotation of solar panel; south = 0°; clockwise [Rad]
#' @param declination Declination (function) [Rad]
#' @param angle_per_second angle change per second (function) [Rad]
#' @return vector: cos of angle of incidence from sun to solar panel [W/m^2]
#'
#' @examples
#' time <- as.POSIXct(mydata$time,format="%Y-%m-%d %H:%M:%S", tz="UTC")
#' declination(time)
#' angle_per_second(time,lon=8*pi/180)
#' fast_cos_incidence(lat=48*pi/180,lon=8*pi/180,tilt=0.3,rot=0.2,declination,angle_per_second)
#' @export
#'
fast_cos_incidence<- function(lat,lon,tilt,rot,declination,angle_per_second,debug=FALSE){

  ca<- sin(declination)*(sin(lat)*cos(tilt)-cos(lat)*cos(rot)*sin(tilt)) + cos(declination)*(sin(lat)*cos(rot)*sin(tilt)+cos(tilt)*cos(lat))*cos(angle_per_second) + sin(rot)*sin(tilt)*cos(declination)*sin(angle_per_second)

  return(ca)
}
